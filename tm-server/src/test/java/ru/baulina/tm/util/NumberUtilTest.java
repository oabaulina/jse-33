package ru.baulina.tm.util;

import org.jetbrains.annotations.NotNull;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.rules.TestName;
import ru.baulina.tm.UnitTest;
import ru.baulina.tm.UnitUtilTest;

@Category({UnitTest.class, UnitUtilTest.class})
public class NumberUtilTest {

    @Rule
    @NotNull
    public final TestName testName = new TestName();

    @Before
    public void setTestName() throws Exception {
        System.out.println(testName.getMethodName());
    }

    @Test
    public void testFormatBytes() {
        final long bytes = 1000;
        @NotNull final String expected = "1000 B";
        Assert.assertEquals(expected, NumberUtil.formatBytes(bytes));
    }

    @Test
    public void testFormatBytesToKilobytes() {
        final long bytes = 1024L;
        @NotNull final String expected = "1 KB";
        Assert.assertEquals(expected, NumberUtil.formatBytes(bytes));
    }

    @Test
    public void testFormatBytesToMegabytes() {
        final long bytes = 1024L * 1024L;
        @NotNull final String expected = "1 MB";
        Assert.assertEquals(expected, NumberUtil.formatBytes(bytes));
    }

    @Test
    public void testFormatBytesToGigabytes() {
        final long bytes = 1024L * 1024L * 1024L;
        @NotNull final String expected = "1 GB";
        Assert.assertEquals(expected, NumberUtil.formatBytes(bytes));
    }

    @Test
    public void testFormatBytesToTerabytes() {
        final Long bytes = 1024L * 1024L * 1024L * 1024L;
        @NotNull final String expected = "1 TB";
        Assert.assertEquals(expected, NumberUtil.formatBytes(bytes));
    }

}