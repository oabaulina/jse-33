package ru.baulina.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.baulina.tm.api.repository.IRepository;
import ru.baulina.tm.entity.AbstractEntity;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import java.util.List;

public abstract class AbstractRepository<E extends AbstractEntity> implements IRepository<E> {

    @NotNull
    private final Class<E> clazz;

    @NotNull
    @PersistenceContext
    protected EntityManager entityManager;

    public AbstractRepository(
            @NotNull final Class<E> clazz
    ) {
        this.clazz = clazz;
    }

    @NotNull
    @Override
    public List<E> findAll(){
        return entityManager
                .createQuery("FROM " + clazz.getName())
                .getResultList();
    }

    @Override
    public void merge(@Nullable final List<E> entities) {
        if (entities == null) return;
        for (final E entity: entities) merge(entity);
    }

    @Override
    @SafeVarargs
    public final void merge(@Nullable final E... entities) {
        if (entities == null) return;
        for (final E entity: entities) merge(entity);
    }

    @Override
    public void merge(@Nullable final E entity) {
        if (entity == null) return;
        entityManager.merge(entity);
    }

    @Override
    public void persist(E entity){
        entityManager.persist(entity);
    }

    @Override
    public void save(E entity){
        entityManager.persist(entity);
    }

    @Override
    public void update(E entity){
        entityManager.merge(entity);
    }

    @Override
    public void clear() {
        @NotNull final List<E> entites = findAll();
        entites.forEach(entityManager::remove);
    }

    @NotNull
    @Override
    public Long count(){
        return (Long) entityManager
                .createQuery("SELECT count(t) FROM " + clazz.getName() + " t")
                .getSingleResult();
    }

    public <E> E getEntity(TypedQuery<E> query) {
        final List<E> resultList = query.getResultList();
        if (resultList.isEmpty()) return null;
        return resultList.get(0);
    }

    public List<E> getEntities(TypedQuery<E> query) {
        final List<E> resultList = query.getResultList();
        if (resultList.isEmpty()) return null;
        return resultList;
    }

}
