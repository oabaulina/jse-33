package ru.baulina.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Repository;
import ru.baulina.tm.api.repository.ITaskRepository;
import ru.baulina.tm.entity.Task;

import javax.persistence.TypedQuery;
import java.util.List;

@Repository
@Scope("prototype")
public final class TaskRepository extends AbstractRepository<Task> implements ITaskRepository {

    @Autowired
    public TaskRepository() {
        super(Task.class);
    }

    @Override
    public void remove(@NotNull final Task task) {
        @NotNull final Long id = task.getId();
        entityManager.remove(entityManager.find(Task.class, id));
    }

    @NotNull
    @Override
    public List<Task> findAll(@NotNull final Long userId) {
        @NotNull final String sql = "SELECT t FROM Task t WHERE t.user.id = :userId";
        @NotNull final TypedQuery<Task> query =
                entityManager.createQuery(sql, Task.class)
                        .setParameter("userId", userId);
        return getEntities(query);
    }

    @NotNull
    @Override
    public List<Task> findListTasks() {
        @NotNull final String sql = "SELECT t FROM Task t";
        @NotNull final TypedQuery<Task> query =
                entityManager.createQuery(sql, Task.class);
        return getEntities(query);
    }

    @Override
    public void clear(@NotNull final Long userId) {
        @NotNull final List<Task> tasks = findAll(userId);
        tasks.forEach(entityManager::remove);
    }

    @Nullable
    @Override
    public Task findOneById(
            @NotNull final Long userId,
            @NotNull final Long projectId,
            @NotNull final Long id
    ) {
        @NotNull final String sql = "SELECT t FROM Task t " +
                                    "WHERE t.user.id = :userId AND t.project.id = :projectId AND t.id = :id";
        @NotNull final TypedQuery<Task> query =
                entityManager.createQuery(sql, Task.class)
                        .setParameter("userId", userId)
                        .setParameter("projectId", projectId)
                        .setParameter("id", id);
        return getEntity(query.setMaxResults(1));
    }

    @Nullable
    @Override
    public Task findOneByIndex(
            @NotNull final Long userId,
            @NotNull final Long projectId,
            @NotNull final Integer index
    ) {
        @NotNull final String sql = "SELECT t FROM Task t " +
                                    "WHERE t.user.id = :userId AND t.project.id = :projectId ORDER BY t.id";
        @NotNull final TypedQuery<Task> query =
                entityManager.createQuery(sql, Task.class)
                        .setParameter("userId", userId)
                        .setParameter("projectId", projectId);
        return getEntities(query).get(index);
    }

    @Nullable
    @Override
    public Task findOneByName(
            @NotNull final Long userId,
            @NotNull final Long projectId,
            @NotNull final String name
    ) {
        @NotNull final String sql = "SELECT t FROM Task t " +
                                    "WHERE t.user.id = :userId AND t.project.id = :projectId AND t.name = :name";
        @NotNull final TypedQuery<Task> query =
                entityManager.createQuery(sql, Task.class)
                        .setParameter("userId", userId)
                        .setParameter("projectId", projectId)
                        .setParameter("name", name);
        return getEntity(query.setMaxResults(1));
    }

    @Nullable
    @Override
    public Task removeOneById(
            @NotNull final Long userId,
            @NotNull final Long projectId,
            @NotNull final Long id
    ) {
        @Nullable final Task task = findOneById(userId, projectId, id);
        if (task == null) return null;
        remove(task);
        return task;
    }

    @Nullable
    @Override
    public Task removeOneByIndex(
            @NotNull final Long userId,
            @NotNull final Long projectId,
            @NotNull final Integer index
    ) {
        @Nullable final Task task = findOneByIndex(userId, projectId, index);
        if (task == null) return null;
        remove(task);
        return task;
    }

    @Nullable
    @Override
    public Task removeOneByName(
            @NotNull final Long userId,
            @NotNull final Long projectId,
            @NotNull final String name
    ) {
        @Nullable final Task task = findOneByName(userId, projectId, name);
        if (task == null) return null;
        remove(task);
        return task;
    }

}
