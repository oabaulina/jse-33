package ru.baulina.tm.exception.empty;

public class EmptyNameException extends RuntimeException{

    public EmptyNameException() {
        super("Error! Name is empty...");
    }

}
