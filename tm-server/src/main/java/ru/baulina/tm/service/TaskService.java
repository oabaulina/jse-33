package ru.baulina.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.baulina.tm.api.repository.ITaskRepository;
import ru.baulina.tm.api.service.ITaskService;
import ru.baulina.tm.entity.Project;
import ru.baulina.tm.entity.Task;
import ru.baulina.tm.entity.User;
import ru.baulina.tm.exception.empty.*;
import ru.baulina.tm.exception.entity.TaskNotFoundException;
import ru.baulina.tm.exception.incorrect.IncorrectIndexException;

import java.util.List;

@Service
public final class TaskService extends AbstractService<Task> implements ITaskService {

    @NotNull
    protected ITaskRepository taskRepository;

    @NotNull
    @Autowired
    protected TaskService(@NotNull final ITaskRepository taskRepository) {
        super(taskRepository);
        this.taskRepository = taskRepository;
    }

    @Nullable
    @Override
    @Transactional
    public Task create(
            @Nullable final User user,
            @Nullable final Project project,
            @Nullable final String name
    ) {
        if (user == null) throw new EmptyUserException();
        if (project == null) throw new EmptyProjectException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        @NotNull final Task task = new Task();
        task.setUser(user);
        task.setProject(project);
        task.setName(name);
        taskRepository.merge(task);
        return task;
    }
    
    @Nullable
    @Override
    @Transactional
    public Task create(
            @Nullable final User user, @Nullable final Project project,
            @Nullable final String name, @Nullable final String description
    ) {
        if (user == null) throw new EmptyUserException();
        if (project == null) throw new EmptyProjectException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        if (description == null || description.isEmpty()) throw new EmptyDescriptionException();
        @NotNull final Task task = new Task();
        task.setUser(user);
        task.setProject(project);
        task.setName(name);
        task.setDescription(description);
        taskRepository.merge(task);
        return task;
    }

    @Override
    @Transactional
    public void clear(@Nullable final Long userId) {
        if (userId == null || userId < 0) throw new EmptyUserIdException();
        taskRepository.clear(userId);
    }

    @Nullable
    @Override
    @Transactional(readOnly = true)
    public List<Task> findAll(@Nullable final Long userId) {
        if (userId == null || userId < 0) throw new EmptyUserIdException();
        return taskRepository.findAll(userId);
    }

    @Nullable
    @Override
    @Transactional(readOnly = true)
    public List<Task> findListTasks() {
        return taskRepository.findListTasks();
    }

    @Nullable
    @Override
    @Transactional(readOnly = true)
    public Task findOneById(
            @Nullable final Long userId, 
            @Nullable final Long projectId, 
            @Nullable final Long id
    ) {
        if (userId == null || userId < 0) throw new EmptyUserIdException();
        if (projectId == null || projectId < 0) throw new EmptyProjectIdException();
        if (id == null || id < 0) throw new EmptyIdException();
        return taskRepository.findOneById(userId, projectId, id);
    }

    @Nullable
    @Override
    @Transactional(readOnly = true)
    public Task findOneByIndex(
            @Nullable final Long userId,
            @Nullable final Long projectId,
            @Nullable final Integer index
    ) {
        if (userId == null || userId < 0) throw new EmptyUserIdException();
        if (projectId == null || projectId < 0) throw new EmptyProjectIdException();
        if (index == null || index < 0) throw new IncorrectIndexException();
        return taskRepository.findOneByIndex(userId, projectId, index);
    }

    @Nullable
    @Override
    @Transactional(readOnly = true)
    public Task findOneByName(
            @Nullable final Long userId,
            @Nullable final Long projectId,
            @Nullable final String name
    ) {
        if (userId == null || userId < 0) throw new EmptyUserIdException();
        if (projectId == null || projectId < 0) throw new EmptyProjectIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        return taskRepository.findOneByName(userId, projectId, name);
    }

    @Override
    @Transactional
    public void remove(
            @Nullable final Task task
    ) {
        if (task == null) throw new EmptyTaskException();
        taskRepository.remove(task);
    }

    @Override
    @Transactional
    public void removeOneById(
            @Nullable final Long userId,
            @Nullable final Long projectId,
            @Nullable final Long id
    ) {
        if (userId == null || userId < 0) throw new EmptyUserIdException();
        if (projectId == null || projectId < 0) throw new EmptyProjectIdException();
        if (id == null || id < 0) throw new EmptyIdException();
        @Nullable Task task = taskRepository.removeOneById(userId, projectId, id);
        if (task == null) throw new TaskNotFoundException();
    }

    @Override
    @Transactional
    public void removeOneByIndex(
            @Nullable final Long userId,
            @Nullable final Long projectId,
            @Nullable final Integer index
    ) {
        if (userId == null || userId < 0) throw new EmptyUserIdException();
        if (projectId == null || projectId < 0) throw new EmptyProjectIdException();
        if (index == null || index < 0) throw new IncorrectIndexException();
        @Nullable Task task = taskRepository.removeOneByIndex(userId, projectId, index);
        if (task == null) throw new TaskNotFoundException();
    }

    @Override
    @Transactional
    public void removeOneByName(
            @Nullable final Long userId,
            @Nullable final Long projectId,
            @Nullable final String name
    ) {
        if (userId == null || userId < 0) throw new EmptyUserIdException();
        if (projectId == null || projectId < 0) throw new EmptyProjectIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        @Nullable Task task = taskRepository.removeOneByName(userId, projectId, name);
        if (task == null) throw new TaskNotFoundException();
    }

    @Override
    @Transactional
    public void updateTaskById(
            @Nullable final Long userId, @Nullable final Long projectId,
            @Nullable final Long id,
            @Nullable final String name, @Nullable final String description
    ) {
        if (userId == null || userId < 0) throw new EmptyUserIdException();
        if (projectId == null || projectId < 0) throw new EmptyProjectIdException();
        if (id == null || id < 0) throw new EmptyIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        if (description == null || description.isEmpty()) throw new EmptyDescriptionException();
        final Task task = findOneById(userId, projectId, id);
        if (task == null) throw new TaskNotFoundException();
        task.setId(id);
        task.setName(name);
        task.setDescription(description);
        taskRepository.merge(task);
    }

    @Override
    @Transactional
    public void updateTaskByIndex(
            @Nullable final Long userId, @Nullable final Long projectId,
            @Nullable final Integer index,
            @Nullable final String name, @Nullable final String description
    ) {
        if (userId == null || userId < 0) throw new EmptyUserIdException();
        if (projectId == null || projectId < 0) throw new EmptyProjectIdException();
        if (index == null || index < 0) throw new IncorrectIndexException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        if (description == null || description.isEmpty()) throw new EmptyDescriptionException();
        @Nullable final Task task = findOneByIndex(userId, projectId, index);
        if (task == null) throw new TaskNotFoundException();
        task.setName(name);
        task.setDescription(description);
        taskRepository.merge(task);
    }

}
