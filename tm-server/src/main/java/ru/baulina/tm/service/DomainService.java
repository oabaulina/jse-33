package ru.baulina.tm.service;

import lombok.RequiredArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.baulina.tm.api.service.IDomainService;
import ru.baulina.tm.api.service.IProjectService;
import ru.baulina.tm.api.service.ITaskService;
import ru.baulina.tm.api.service.IUserService;
import ru.baulina.tm.dto.DomainDTO;

@Service
@RequiredArgsConstructor
public class DomainService implements IDomainService {

    @NotNull
    @Autowired
    private final IUserService userService;

    @NotNull
    @Autowired
    private final IProjectService projectService;

    @NotNull
    @Autowired
    private final ITaskService taskService;

    @Override
    public void load(@Nullable final DomainDTO domain) {
        if (domain == null) return;
        projectService.load(domain.getProjects());
        taskService.load(domain.getTasks());
        userService.load(domain.getUsers());
    }

    @Override
    public void export(@Nullable final DomainDTO domain) {
        if (domain == null) return;
        domain.setProjects(projectService.findListProjects());
        domain.setTasks(taskService.findListTasks());
        domain.setUsers(userService.findListUsers());
    }

}
