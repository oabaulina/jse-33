package ru.baulina.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import ru.baulina.tm.api.endpoint.ITaskEndpoint;
import ru.baulina.tm.api.service.IProjectService;
import ru.baulina.tm.api.service.ISessionService;
import ru.baulina.tm.api.service.ITaskService;
import ru.baulina.tm.api.service.IUserService;
import ru.baulina.tm.dto.SessionDTO;
import ru.baulina.tm.dto.TaskDTO;
import ru.baulina.tm.entity.Project;
import ru.baulina.tm.entity.Task;
import ru.baulina.tm.entity.User;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.ArrayList;
import java.util.List;

@WebService
@Controller
public class TaskEndpoint extends AbstractEndpoint implements ITaskEndpoint {

    @Nullable
    @Autowired
    private ISessionService sessionService;

    @Nullable
    @Autowired
    private IUserService userService;

    @Nullable
    @Autowired
    private ITaskService taskService;

    @Nullable
    @Autowired
    private IProjectService projectService;

    @Override
    @WebMethod
    public @NotNull List<TaskDTO> getTaskList() {
        @Nullable final List<Task> taskList = taskService.findAll();
        @Nullable List<TaskDTO> taskListDTO = new ArrayList<>();
        taskList.forEach((task) -> taskListDTO.add((new TaskDTO()).taskDTOfrom(task)));
        return taskListDTO;
    }

    @Override
    @WebMethod
    public void loadTasks(
            @WebParam(name = "tasks", partName = "tasks") List<Task> tasks
    ) {
        taskService.load(tasks);
    }

    @Override
    @WebMethod
    public TaskDTO createTask(
            @WebParam(name = "session", partName = "session") final SessionDTO session,
            @WebParam(name = "projectId", partName = "projectId") final Long projectId,
            @WebParam(name = "name", partName = "name") String name
    ) {
        sessionService.validate(session);
        @Nullable Long userId = session.getUserId();
        @Nullable final Project project = projectService.findOneById(userId, projectId);
        @Nullable final User user = userService.findById(userId);
        @Nullable final Task task = taskService.create(user, project, name);
        @Nullable TaskDTO taskDTO = new TaskDTO();
        return taskDTO.taskDTOfrom(task);
    }

    @Override
    @WebMethod
    public TaskDTO createTaskWithDescription(
            @WebParam(name = "session", partName = "session") final SessionDTO session,
            @WebParam(name = "projectId", partName = "projectId") final Long projectId,
            @WebParam(name = "name", partName = "name") String name,
            @WebParam(name = "description", partName = "description") String description
    ) {
        sessionService.validate(session);
        @Nullable Long userId = session.getUserId();
        @Nullable final Project project = projectService.findOneById(userId, projectId);
        @Nullable final User user = userService.findById(userId);
        @Nullable final Task task = taskService.create(user, project, name, description);
        @Nullable TaskDTO taskDTO = new TaskDTO();
        return taskDTO.taskDTOfrom(task);
    }

    @Override
    @WebMethod
    public void removeTask(
            @WebParam(name = "session", partName = "session") final SessionDTO session,
            @WebParam(name = "projectId", partName = "projectId") final Long projectId,
            @WebParam(name = "task", partName = "task") Task task
    ) {
        sessionService.validate(session);
        taskService.remove(task);
    }

    @Override
    @WebMethod
    public List<TaskDTO> findAllTasks(
            @WebParam(name = "session", partName = "session") final SessionDTO session
    ) {
        sessionService.validate(session);
        @Nullable Long userId = session.getUserId();
        @Nullable final List<Task> taskList = taskService.findAll(userId);
        @Nullable List<TaskDTO> taskListDTO = new ArrayList<>();
        taskList.forEach((task) -> taskListDTO.add((new TaskDTO()).taskDTOfrom(task)));
        return taskListDTO;
    }

    @Override
    @WebMethod
    public void clearTasks(
            @WebParam(name = "session", partName = "session") final SessionDTO session
    ) {
        sessionService.validate(session);
        taskService.clear(session.getUserId());
    }

    @Override
    @WebMethod
    public TaskDTO findOneTaskById(
            @WebParam(name = "session", partName = "session") final SessionDTO session,
            @WebParam(name = "projectId", partName = "projectId") final Long projectId,
            @WebParam(name = "id", partName = "id") Long id
    ) {
        sessionService.validate(session);
        @Nullable TaskDTO taskDTO = new TaskDTO();
        @Nullable Long userId = session.getUserId();
        @Nullable final Task task = taskService.findOneById(userId, projectId, id);
        return taskDTO.taskDTOfrom(task);
    }

    @Override
    @WebMethod
    public TaskDTO findOneTaskByIndex(
            @WebParam(name = "session", partName = "session") final SessionDTO session,
            @WebParam(name = "projectId", partName = "projectId") final Long projectId,
            @WebParam(name = "index", partName = "index") Integer index
    ) {
        sessionService.validate(session);
        @Nullable TaskDTO taskDTO = new TaskDTO();
        @Nullable Long userId = session.getUserId();
        @Nullable final Task task = taskService.findOneByIndex(userId, projectId, index);
        return taskDTO.taskDTOfrom(task);
    }

    @Override
    @WebMethod
    public TaskDTO findOneTaskByName(
            @WebParam(name = "session", partName = "session") final SessionDTO session,
            @WebParam(name = "projectId", partName = "projectId") final Long projectId,
            @WebParam(name = "name", partName = "name") String name
    ) {
        sessionService.validate(session);
        @Nullable TaskDTO taskDTO = new TaskDTO();
        @Nullable Long userId = session.getUserId();
        @Nullable final Task task = taskService.findOneByName(userId, projectId, name);
        return taskDTO.taskDTOfrom(task);
    }

    @Override
    @WebMethod
    public void removeOneTaskById(
            @WebParam(name = "session", partName = "session") final SessionDTO session,
            @WebParam(name = "projectId", partName = "projectId") final Long projectId,
            @WebParam(name = "id", partName = "id") Long id
    ) {
        sessionService.validate(session);
        @Nullable Long userId = session.getUserId();
        taskService.removeOneById(userId, projectId, id);
    }

    @Override
    @WebMethod
    public void removeOneTaskByIndex(
            @WebParam(name = "session", partName = "session") final SessionDTO session,
            @WebParam(name = "projectId", partName = "projectId") final Long projectId,
            @WebParam(name = "index", partName = "index") Integer index
    ) {
        sessionService.validate(session);
        @Nullable Long userId = session.getUserId();
        taskService.removeOneByIndex(userId, projectId, index);
    }

    @Override
    @WebMethod
    public void removeOneTaskByName(
            @WebParam(name = "session", partName = "session") final SessionDTO session,
            @WebParam(name = "projectId", partName = "projectId") final Long projectId,
            @WebParam(name = "name", partName = "name") String name
    ) {
        sessionService.validate(session);
        @Nullable Long userId = session.getUserId();
        taskService.removeOneByName(userId, projectId, name);
    }

    @Override
    @WebMethod
    public void updateTaskById(
            @WebParam(name = "session", partName = "session") final SessionDTO session,
            @WebParam(name = "projectId", partName = "projectId") final Long projectId,
            @WebParam(name = "id", partName = "id") Long id,
            @WebParam(name = "name", partName = "name") String name,
            @WebParam(name = "description", partName = "description") String description
    ) {
        sessionService.validate(session);
        @Nullable Long userId = session.getUserId();
        projectService.findOneById(userId, projectId);
        taskService.updateTaskById(userId, projectId, id, name, description);
    }

    @Override
    @WebMethod
    public void updateTaskByIndex(
            @WebParam(name = "session", partName = "session") final SessionDTO session,
            @WebParam(name = "projectId", partName = "projectId") final Long projectId,
            @WebParam(name = "index", partName = "index") Integer index,
            @WebParam(name = "name", partName = "name") String name,
            @WebParam(name = "description", partName = "description") String description
    ) {
        sessionService.validate(session);
        @Nullable Long userId = session.getUserId();
        taskService.updateTaskByIndex(userId, projectId, index, name, description);
    }

}
