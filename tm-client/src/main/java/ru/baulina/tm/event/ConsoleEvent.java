package ru.baulina.tm.event;

import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.springframework.context.ApplicationEvent;

@Getter
@Setter
public class ConsoleEvent extends ApplicationEvent {

    @NotNull
    private String name;

    public ConsoleEvent(Object source, @NotNull String name) {
        super(source);
        this.name = name;
    }

}
