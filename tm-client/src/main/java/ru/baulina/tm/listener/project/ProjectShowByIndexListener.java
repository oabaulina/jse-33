package ru.baulina.tm.listener.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.EventListener;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;
import ru.baulina.tm.endpoint.ProjectEndpoint;
import ru.baulina.tm.endpoint.SessionDTO;
import ru.baulina.tm.event.ConsoleEvent;
import ru.baulina.tm.util.TerminalUtil;

@Component
public class ProjectShowByIndexListener extends AbstractProjectListener {

    @Autowired
    private ProjectEndpoint projectEndpoint;

    @NotNull
    @Override
    public String name() {
        return "project-show-by-index";
    }

    @NotNull
    @Override
    public String description() {
        return "Show project by index.";
    }

    @Override
    @Async("asyncExecutor")
    @EventListener(condition = "@projectShowByIndexListener.name() == #event.name")
    public void handler(@NotNull final ConsoleEvent event) {
        System.out.println("[SHOW PROJECT]");
        @Nullable final Integer index;
        synchronized(TerminalUtil.class) {
            System.out.println("ENTER INDEX");
            index = TerminalUtil.nexInt() - 1;
        }
        @Nullable final SessionDTO session = getSession();
        projectEndpoint.removeOneProjectByIndex(session, index);
        System.out.println("[OK]");
        System.out.println();
    }

}
