package ru.baulina.tm.listener.system;

import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.EventListener;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;
import ru.baulina.tm.api.service.ICommandService;
import ru.baulina.tm.event.ConsoleEvent;
import ru.baulina.tm.listener.AbstractListener;

import java.util.Arrays;
import java.util.List;

@Component
public class ArgumentShowListener extends AbstractListener {

    @Autowired
    private ICommandService commandService;

    @NotNull
    @Override
    public String arg() {
        return "-arg";
    }

    @NotNull
    @Override
    public String name() {
        return "arguments";
    }

    @NotNull
    @Override
    public String description() {
        return "Show program arguments.";
    }

    @Override
    @Async("asyncExecutor")
    @EventListener(condition = "@argumentShowListener.name() == #event.name")
    public void handler(@NotNull final ConsoleEvent event) {
        argumentShow();
    }

    @Async("asyncExecutor")
    @EventListener(condition = "@argumentShowListener.arg() == #event.name")
    public void handlerArg(@NotNull final ConsoleEvent event) {
        argumentShow();
    }

    private void argumentShow() {
        @NotNull final List<String> arguments = commandService.getArgs();
        System.out.println(Arrays.toString(arguments.toArray()));
        System.out.println();
    }
}
