package ru.baulina.tm.listener.user;

import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.EventListener;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;
import ru.baulina.tm.api.service.ISessionDTOService;
import ru.baulina.tm.endpoint.SessionDTO;
import ru.baulina.tm.endpoint.SessionEndpoint;
import ru.baulina.tm.event.ConsoleEvent;
import ru.baulina.tm.util.TerminalUtil;

@Component
public class SessionOpenListener extends AbstractUserListener {

    @Autowired
    private SessionEndpoint sessionEndpoint;

    @Autowired
    private ISessionDTOService sessionDTOService;

    @NotNull
    @Override
    public String name() {
        return "open-session";
    }

    @NotNull
    @Override
    public String description() {
        return "Open session.";
    }

    @Override
    @Async("asyncExecutor")
    @EventListener(condition = "@sessionOpenListener.name() == #event.name")
    public void handler(@NotNull final ConsoleEvent event) {
        System.out.println("[OPEN_SESSION]");
        @NotNull final String login;
        @NotNull final String password;
        synchronized(TerminalUtil.class) {
            System.out.println("ENTER LOGIN:");
            login = TerminalUtil.nextLine();
            System.out.println("ENTER PASSWORD:");
            password = TerminalUtil.nextLine();
        }
        final SessionDTO session = sessionEndpoint.openSession(login, password);
        sessionDTOService.setSession(session);
        System.out.println("[OK]");
        System.out.println();
    }

}
