package ru.baulina.tm.listener.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.EventListener;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;
import ru.baulina.tm.endpoint.SessionDTO;
import ru.baulina.tm.endpoint.TaskDTO;
import ru.baulina.tm.endpoint.TaskEndpoint;
import ru.baulina.tm.event.ConsoleEvent;
import ru.baulina.tm.util.TerminalUtil;

@Component
public class TaskShowByIdListener extends AbstractTaskListener {

    @Autowired
    private TaskEndpoint taskEndpoint;

    @NotNull
    @Override
    public String name() {
        return "task-show-by-id";
    }

    @NotNull
    @Override
    public String description() {
        return "Show task by id.";
    }

    @Override
    @Async("asyncExecutor")
    @EventListener(condition = "@taskShowByIdListener.name() == #event.name")
    public void handler(@NotNull final ConsoleEvent event) {
        System.out.println("[SHOW TASK]");
        @Nullable final Long id;
        @Nullable final Long projectId;
        synchronized(TerminalUtil.class) {
            System.out.println("ENTER ID");
            id = TerminalUtil.nexLong();
            System.out.println("ENTER PROJECT ID:");
            projectId = TerminalUtil.nexLong();
        }
        @Nullable final SessionDTO session = getSession();
        @Nullable final TaskDTO task = taskEndpoint.findOneTaskById(session, projectId, id);
        if (task == null) {
            System.out.println("[FAIL]");
            return;
        }
        System.out.println("ID: " + task.getId());
        System.out.println("NAME: " + task.getName());
        System.out.println("DESCRIPTION: " + task.getDescription());
        System.out.println("[OK]");
        System.out.println();
    }

}
