package ru.baulina.tm.listener.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.EventListener;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;
import ru.baulina.tm.endpoint.*;
import ru.baulina.tm.event.ConsoleEvent;
import ru.baulina.tm.util.TerminalUtil;

@Component
public class TaskShowByIndexListener extends AbstractTaskListener {

    @Autowired
    private TaskEndpoint taskEndpoint;

    @NotNull
    @Override
    public String name() {
        return "task-show-by-index";
    }

    @NotNull
    @Override
    public String description() {
        return "Show task by index.";
    }

    @Override
    @Async("asyncExecutor")
    @EventListener(condition = "@taskShowByIndexListener.name() == #event.name")
    public void handler(@NotNull final ConsoleEvent event) {
        System.out.println("[SHOW TASK]");
        System.out.println("ENTER INDEX");
        @Nullable final Integer index = TerminalUtil.nexInt() -1;
        System.out.println("ENTER PROJECT ID:");
        @Nullable final Long projectId = TerminalUtil.nexLong();
        @Nullable final SessionDTO session = getSession();
        @Nullable final TaskDTO task = taskEndpoint.findOneTaskByIndex(session, projectId, index);
        if (task == null) {
            System.out.println("[FAIL]");
            return;
        }
        System.out.println("ID: " + task.getId());
        System.out.println("NAME: " + task.getName());
        System.out.println("DESCRIPTION: " + task.getDescription());
        System.out.println("[OK]");
        System.out.println();
    }

}
