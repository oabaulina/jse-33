package ru.baulina.tm.listener.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.EventListener;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;
import ru.baulina.tm.endpoint.SessionDTO;
import ru.baulina.tm.endpoint.TaskEndpoint;
import ru.baulina.tm.event.ConsoleEvent;
import ru.baulina.tm.util.TerminalUtil;

@Component
public class TaskUpdateByIndexListener extends AbstractTaskListener {

    @Autowired
    private TaskEndpoint taskEndpoint;

    @NotNull
    @Override
    public String name() {
        return "task-update_by_index";
    }

    @NotNull
    @Override
    public String description() {
        return "Update task by index.";
    }

    @Override
    @Async("asyncExecutor")
    @EventListener(condition = "@taskUpdateByIndexListener.name() == #event.name")
    public void handler(@NotNull final ConsoleEvent event) {
        System.out.println("[UPDATE TASK]");
        @Nullable final Integer index;
        @Nullable final Long projectId;
        @Nullable final String name;
        @Nullable final String description;
        synchronized(TerminalUtil.class) {
            System.out.println("ENTER INDEX:");
            index = TerminalUtil.nexInt() -1;
            System.out.println("ENTER PROJECT ID:");
            projectId = TerminalUtil.nexLong();
            System.out.println("ENTER NAME:");
            name = TerminalUtil.nextLine();
            System.out.println("ENTER DESCRIPTION:");
            description = TerminalUtil.nextLine();
        }
        @Nullable final SessionDTO session = getSession();
        taskEndpoint.removeOneTaskByIndex(session, projectId, index);
        taskEndpoint.updateTaskByIndex(session, projectId, index, name, description);
        System.out.println("[OK]");
        System.out.println();
    }

}
