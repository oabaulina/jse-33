package ru.baulina.tm.listener.system;

import org.jetbrains.annotations.NotNull;
import org.springframework.context.event.EventListener;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;
import ru.baulina.tm.event.ConsoleEvent;
import ru.baulina.tm.listener.AbstractListener;

@Component
public class VersionListener extends AbstractListener {

    @NotNull
    @Override
    public String arg() {
        return "-v";
    }

    @NotNull
    @Override
    public String name() {
        return "version";
    }

    @NotNull
    @Override
    public String description() {
        return "Display program version.";
    }

    @Override
    @Async("asyncExecutor")
    @EventListener(condition = "@versionListener.name() == #event.name")
    public void handler(@NotNull final ConsoleEvent event) {
        version();
    }

    @Async("asyncExecutor")
    @EventListener(condition = "@versionListener.arg() == #event.name")
    public void handlerArg(@NotNull final ConsoleEvent event) {
        version();
    }

    private void version() {
        System.out.println("[ABOUT]");
        System.out.println("Olga Baulina");
        System.out.println("golovolomkacom@gmail.com");
       System.out.println("[VERSION]");
        System.out.println("1.0.33");
        System.out.println("OK");
        System.out.println();
    }

}
