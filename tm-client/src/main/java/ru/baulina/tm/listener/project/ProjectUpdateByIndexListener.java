package ru.baulina.tm.listener.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.EventListener;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;
import ru.baulina.tm.endpoint.ProjectEndpoint;
import ru.baulina.tm.endpoint.SessionDTO;
import ru.baulina.tm.event.ConsoleEvent;
import ru.baulina.tm.util.TerminalUtil;

@Component
public class ProjectUpdateByIndexListener extends AbstractProjectListener {

    @Autowired
    private ProjectEndpoint projectEndpoint;

    @NotNull
    @Override
    public String name() {
        return "project-update_by_index";
    }

    @NotNull
    @Override
    public String description() {
        return "Update project by index.";
    }

    @Override
    @Async("asyncExecutor")
    @EventListener(condition = "@projectUpdateByIndexListener.name() == #event.name")
    public void handler(@NotNull final ConsoleEvent event) {
        System.out.println("[UPDATE PROJECT]");
        @Nullable final Integer index;
        @Nullable final SessionDTO session;
        @Nullable final String name;
        @Nullable final String description;
        synchronized(TerminalUtil.class) {
            System.out.println("ENTER INDEX:");
            index = TerminalUtil.nexInt() - 1;
            session = getSession();
            projectEndpoint.removeOneProjectByIndex(session, index);
            System.out.println("ENTER NAME:");
            name = TerminalUtil.nextLine();
            System.out.println("ENTER DESCRIPTION:");
            description = TerminalUtil.nextLine();
        }
        projectEndpoint.updateProjectByIndex(session, index, name, description);
        System.out.println("[OK]");
        System.out.println();
    }

}
