package ru.baulina.tm.listener.data;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.EventListener;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;
import ru.baulina.tm.api.service.ISessionDTOService;
import ru.baulina.tm.endpoint.AdminDumpEndpoint;
import ru.baulina.tm.endpoint.SessionDTO;
import ru.baulina.tm.endpoint.SessionEndpoint;
import ru.baulina.tm.event.ConsoleEvent;

@Component
public class DataBinaryLoadListener extends AbstractDataListener {

    @Autowired
    private AdminDumpEndpoint adminDumpEndpoint;

    @Autowired
    private SessionEndpoint sessionEndpoint;

    @Autowired
    private ISessionDTOService sessionDTOService;

    @NotNull
    @Override
    public String name() {
        return "data-bin-load";
    }

    @NotNull
    @Override
    public String description() {
        return "Load data from binary file.";
    }

    @Override
    @Async("asyncExecutor")
    @EventListener(condition = "@dataBinaryLoadListener.name() == #event.name")
    public void handler(@NotNull final ConsoleEvent event) {
        System.out.println("[DATA BINARY LOAD]");
        @Nullable final SessionDTO session = getSession();
        adminDumpEndpoint.dataBinaryLoad(session);
        System.out.println("[LOGOUT CURRENT USER]");
        sessionEndpoint.closeSession(session);
        sessionDTOService.setSession(null);
        System.out.println("[OK]");
        System.out.println();
    }

}
